/*
  gpcxx/eval/regression_fitness.hpp

  Copyright 2013 Karsten Ahnert

  Distributed under the Boost Software License, Version 1.0.
  (See accompanying file LICENSE_1_0.txt or
  copy at http://www.boost.org/LICENSE_1_0.txt)
*/


#ifndef GPCXX_EVAL_PARALLEL_REGRESSION_FITNESS_HPP_DEFINED
#define GPCXX_EVAL_PARALLEL_REGRESSION_FITNESS_HPP_DEFINED

#include <vector>
#include <cmath>
#include <cstddef>
#include <array>
#include <tbb/parallel_reduce.h>
#include <tbb/blocked_range.h>

namespace gpcxx {


template< typename Value , size_t Dim , typename SequenceType = std::vector< Value > >

struct parallel_regression_training_data
{
    static const size_t dim = Dim;
    SequenceType y;
    std::array< SequenceType , dim > x;
};

template< typename Value , size_t Dim >
using regression_context = std::array< Value , Dim >;


namespace detail {
    
    struct parallel_abs
    {
        template< typename T >
        T operator()( T t ) const { return std::abs( t ); }
    };
    
    struct parallel_square
    {
        template< typename T >
        T operator()( T t ) const { return t * t; }
    };
    
} // namespace detail


template< typename Eval , typename Norm = detail::parallel_abs >
struct parallel_regression_fitness
{
    typedef Eval eval_type;
    typedef typename eval_type::context_type context_type;
    typedef typename eval_type::value_type value_type;
    
    eval_type m_eval;
    
    parallel_regression_fitness( eval_type eval ) : m_eval( eval ) { }
    
    template< typename Tree , typename TrainingData >

    value_type get_chi2( Tree const &t , TrainingData const& c ) const
    {
        auto chi2 = tbb::parallel_reduce(
                   tbb::blocked_range<size_t>(0 , c.x[0].size(), 100),
                   0.0,
                   [&](tbb::blocked_range<size_t> r, double current_chi2)
                    {
                        context_type cc;
                        for( size_t i=r.begin() ; i<r.end(); ++i )
                        {
                            for( size_t j=0 ; j<TrainingData::dim ; ++j )
                            {
                                cc[j] = c.x[j][i];
                            }
                            value_type yy = m_eval( t, cc );
                            current_chi2 += Norm()( yy-c.y[i]);
                        }
                        return current_chi2;
                    },
                    std::plus<value_type>());
        return chi2 / value_type( c.x[0].size() );
    }

    template< typename Tree , typename TrainingData >
    value_type operator()( Tree const & t , TrainingData const& c ) const
    {
        value_type chi2 = get_chi2( t , c );
        return ( std::isnan( chi2 ) ? 1.0 : 1.0 - 1.0 / ( 1.0 + chi2 ) );
    }
};

template< typename Eval >
parallel_regression_fitness< Eval > make_parallel_regression_fitness( Eval eval )
{
    return parallel_regression_fitness< Eval >( eval );
}


} // namespace gpcxx


#endif // GPCXX_EVAL_PARALLEL_REGRESSION_FITNESS_HPP_DEFINED
