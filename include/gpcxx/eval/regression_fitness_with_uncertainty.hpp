/*
  gpcxx/eval/regression_fitness.hpp

  Copyright 2013 Karsten Ahnert

  Distributed under the Boost Software License, Version 1.0.
  (See accompanying file LICENSE_1_0.txt or
  copy at http://www.boost.org/LICENSE_1_0.txt)
*/


#ifndef GPCXX_EVAL_REGRESSION_FITNESS_HPP_WITH_UNCERTAINTY_DEFINED
#define GPCXX_EVAL_REGRESSION_FITNESS_HPP_WITH_UNCERTAINTY_DEFINED

#include <vector>
#include <cmath>
#include <cstddef>
#include <array>

namespace gpcxx {


template< typename Value , size_t Dim , typename SequenceType = std::vector< Value > >
struct regression_training_data_with_uncertainty
{
    static const size_t dim = Dim;
    SequenceType y;
    std::array< SequenceType , dim > x;
};

template< typename Value , size_t Dim >
using regression_context = std::array< Value , Dim >;


namespace detail {
    
    struct abs_with_uncertainty
    {
        template< typename T >
        T operator()( T t ) const { return std::abs( t ); }
    };
    
    struct square_with_uncertainty
    {
        template< typename T >
        T operator()( T t ) const { return t * t; }
    };
    
} // namespace detail
    struct Fitness_values
    {
        Fitness_values(double fitness , double test_fitness , double variance , size_t number_of_bootstraps , std::vector<double> bootstrap_fitness):
                fitness(fitness), test_fitness(test_fitness), variance(variance), number_of_bootstraps(number_of_bootstraps), bootstrap_fitness(bootstrap_fitness) {}
        double fitness;
        double test_fitness;
        double variance;
        size_t number_of_bootstraps;
        std::vector<double> bootstrap_fitness;
    };


template< typename Eval , typename Norm = detail::abs >
struct regression_fitness_with_uncertainty
{
    typedef Eval eval_type;
    typedef typename eval_type::context_type context_type;
    typedef typename eval_type::value_type value_type;
    typedef std::mt19937 rng_type ;
    
    eval_type m_eval;
    
    regression_fitness_with_uncertainty( eval_type eval ) : m_eval( eval ) { }
    
    template< typename Tree , typename TrainingData >
    value_type get_precision( Tree const &t , TrainingData const& c ) const
    {
        value_type precision = 0.0;
	
        for( size_t i=0 ; i<c.x[0].size() ; ++i )
        {
            context_type cc;
            for( size_t j=0 ; j<TrainingData::dim ; ++j ) cc[j] = c.x[j][i];
            value_type yy = m_eval( t , cc );
	    
            precision += Norm()( yy - c.y[i] );
        }
        return precision / value_type( c.x[0].size() );
    }
    template< typename Tree , typename TrainingData >
    std::vector <value_type> get_bootstrap_precision( Tree const &t , TrainingData const& c , rng_type rng , const size_t number_of_bootstraps ) const
    {
        std::vector<value_type> precision_on_bootstraps;

        size_t number_of_samples = c.x[0].size();
        std::uniform_int_distribution<size_t> sample_distribution(0,number_of_samples);

        size_t sample_id;
        //Can be parallelized
        for(size_t k=0 ; k< number_of_bootstraps ; ++k)
        {
            value_type precision = 0.0;

            for( size_t i=0 ; i<number_of_samples ; ++i )
            {
                sample_id = sample_distribution(rng);
                context_type cc;

                for( size_t j=0 ; j<TrainingData::dim ; ++j )
                {

                    cc[j] = c.x[j][sample_id];
                }
                value_type yy = m_eval( t , cc );

                precision += Norm()( yy - c.y[sample_id] );
            }
            precision_on_bootstraps.push_back(precision/number_of_samples);
        }
        return precision_on_bootstraps;
    }
    double get_variance( std::vector <value_type> data) const
    {
        size_t data_size = data.size();
        value_type mean = 0.0;
        for (size_t k=0 ; k<data_size ; ++k)
        {
            mean = mean + data[k];
        }
        mean = mean / data_size;
        value_type variance = 0.0;
        for (size_t k=0 ; k<data_size ; ++k)
        {
            variance += std::pow(mean-data[k],2);
        }
        return variance/data_size;
    }


    template< typename Tree , typename TrainingData >
    Fitness_values operator()( Tree const & t , TrainingData const& c1 , TrainingData const& c2 , rng_type rng, const size_t number_of_bootstraps = 20) const
    {

        std::vector <value_type> bootstrap_precision = get_bootstrap_precision(t , c1 , rng , number_of_bootstraps );
        Fitness_values result(get_precision(t , c1) , get_precision( t , c2) , get_variance( bootstrap_precision ) , number_of_bootstraps , bootstrap_precision);
        return result;
    }
};

template< typename Eval >
regression_fitness_with_uncertainty< Eval > make_regression_fitness_with_uncertainty( Eval eval )
{
  //building
    return regression_fitness_with_uncertainty< Eval >( eval );
}


} // namespace gpcxx


#endif // GPCXX_EVAL_REGRESSION_FITNESS_HPP_WITH_UNCERTAINTY_DEFINED
