/*
 * gpcxx/io/best_individuals_with_test.hpp
 * Date: 2013-02-22
 * Author: Karsten Ahnert (karsten.ahnert@gmx.de)
 */

#ifndef GPCXX_IO_BEST_INDIVIDUALS_WITH_TEST_HPP_INCLUDED
#define GPCXX_IO_BEST_INDIVIDUALS_WITH_TEST_HPP_INCLUDED

#include <gpcxx/util/sort_indices.hpp>
#include <gpcxx/util/indent.hpp>
#include <gpcxx/util/identity.hpp>
#include <gpcxx/io/simple.hpp>

#include <ostream>
#include <vector>

namespace gpcxx {

template< typename Pop , typename Fitness , typename SymbolMapper >
void write_best_individuals_with_test( std::ostream &out , const Pop& p , const Fitness &f , const Fitness &t , const Fitness &u , const std::vector< std::vector< double > > bootstrap, size_t ind , size_t num_individuals , bool write_infix , SymbolMapper const& mapper )
{
    std::vector< size_t > idx;
    gpcxx::sort_indices( f , idx );
    size_t number_of_bootstraps = bootstrap.size();
    for( size_t i=0 ; i<num_individuals ; ++i )
    {
        out << i << ":" << simple( p[ idx[i] ], false, mapper ) << std::endl;
        out << indent( ind ) << f[ idx[i] ] << ";" << t[ idx[i] ]<< ";" << u[ idx[i] ];
        for( size_t k=0 ; k<number_of_bootstraps ; ++k)
        {
            out << ";" << bootstrap[idx[i]][k];
        }
        out << std::endl;
    }
}

template< typename Pop , typename Fitness , typename SymbolMapper >
void print_best_individuals_with_test( std::ostream &out , const Pop& p , const Fitness &f , const Fitness &t , const Fitness &u, size_t ind , size_t num_individuals , bool write_infix , SymbolMapper const& mapper )
{
    std::vector< size_t > idx;
    gpcxx::sort_indices( f , idx );
    bool first = true;
    for( size_t i=0 ; i<num_individuals ; ++i )
    {
        if( first ) first = false; else out << "\n";
        out << indent( ind ) <<  i << ";" << f[ idx[i] ] << ";" << t[ idx[i] ]<< ";" << u[ idx[i] ] << ";" << simple( p[ idx[i] ], false, mapper );
    }
}

namespace detail {
    
template< typename Pop , typename Fitness , typename SymbolMapper >
struct best_individuals_writer_with_test
{
    Pop const& m_pop;
    Fitness const& m_fitness;
    Fitness const& m_test;
    Fitness const& m_uncertainty;
    std::vector< std::vector< double > > const m_bootstrap_precision;
    size_t m_indent;
    size_t m_num_individuals;
    bool m_write_infix;
    SymbolMapper const& m_mapper;
  best_individuals_writer_with_test( Pop const& pop , Fitness const& fitness , Fitness const& test , Fitness const& uncertainty , std::vector< std::vector< double > > const bootstrap_precision, size_t indent , size_t num_individuals , bool write_infix , SymbolMapper const& mapper )
    : m_pop( pop ) , m_fitness( fitness ) , m_test( test ), m_uncertainty (uncertainty) , m_bootstrap_precision( bootstrap_precision ) , m_indent( indent ) , m_num_individuals( num_individuals ) , m_write_infix( write_infix ) , m_mapper( mapper ) { }

    std::ostream& operator()( std::ostream &out ) const
    {
        write_best_individuals_with_test( out , m_pop , m_fitness , m_test , m_uncertainty , m_bootstrap_precision , m_indent , m_num_individuals , m_write_infix , m_mapper );
        return out;
    }
};

template< typename Pop , typename Fitness , typename SymbolMapper >
std::ostream& operator<<( std::ostream &out , best_individuals_writer_with_test< Pop , Fitness , SymbolMapper > const& b )
{
    return b( out );
}

template< typename Pop , typename Fitness , typename SymbolMapper >
struct best_individuals_printer_with_test
{
    Pop const& m_pop;
    Fitness const& m_fitness;
    Fitness const& m_test;
    Fitness const& m_uncertainty;
    size_t m_indent;
    size_t m_num_individuals;
    bool m_write_infix;
    SymbolMapper const& m_mapper;
  best_individuals_printer_with_test( Pop const& pop , Fitness const& fitness , Fitness const& test , Fitness const& uncertainty , size_t indent , size_t num_individuals , bool write_infix , SymbolMapper const& mapper )
    : m_pop( pop ) , m_fitness( fitness ) , m_test( test ), m_uncertainty (uncertainty), m_indent( indent ) , m_num_individuals( num_individuals ) , m_write_infix( write_infix ) , m_mapper( mapper ) { }

    std::ostream& operator()( std::ostream &out ) const
    {
        print_best_individuals_with_test( out , m_pop , m_fitness , m_test , m_uncertainty , m_indent , m_num_individuals , m_write_infix , m_mapper );
        return out;
    }
};


template< typename Pop , typename Fitness , typename SymbolMapper >
std::ostream& operator<<( std::ostream &out , best_individuals_printer_with_test< Pop , Fitness , SymbolMapper > const& b )
{
    return b( out );
}

} // namespace detail

template< typename Pop , typename Fitness , typename SymbolMapper = gpcxx::identity >
detail::best_individuals_printer_with_test< Pop , Fitness , SymbolMapper > print_best_individuals_with_test( Pop const& pop , Fitness const& fitness , Fitness const& test , Fitness const& uncertainty , size_t indent = 0 , size_t num_individuals = 10 , bool write_infix = true , SymbolMapper const &mapper = SymbolMapper() )
{
    return detail::best_individuals_printer_with_test< Pop , Fitness , SymbolMapper >( pop , fitness , test , uncertainty , indent , num_individuals , write_infix , mapper );
}

template< typename Pop , typename Fitness , typename SymbolMapper = gpcxx::identity >
detail::best_individuals_writer_with_test< Pop , Fitness , SymbolMapper > write_best_individuals_with_test( Pop const& pop , Fitness const& fitness , Fitness const& test , Fitness const& uncertainty , std::vector< std::vector< double > > bootstrap_precision, size_t indent = 0 , size_t num_individuals = 10 , bool write_infix = true , SymbolMapper const &mapper = SymbolMapper() )
{
    return detail::best_individuals_writer_with_test< Pop , Fitness , SymbolMapper >( pop , fitness , test , uncertainty , bootstrap_precision , indent , num_individuals , write_infix , mapper );
}

} // namespace gpcxx

#endif // GPCXX_STAT_BEST_INDIVIDUALS_HPP_INCLUDED
