
#ifndef GPCXX_IO_READ_DATA_HPP_INCLUDED
#define GPCXX_IO_READ_DATA_HPP_INCLUDED

#include <iostream>
#include <string>
#include <sstream>
#include <fstream>
#include <gpcxx/eval/regression_fitness.hpp>

namespace gpcxx
{
gpcxx::regression_training_data< double , 5> read_trajectory_from_csv(std::string path)
{
    std::ifstream csv_file;
    csv_file.open(path);
    size_t num_of_points = 0;
    std::string line;
    std::string s_x;
    std::string s_dx;
    std::string s_theta;
    std::string s_dtheta;
    std::string s_u;
    std::string to_throw;
    std::vector<double> x;
    std::vector<double> dx;
    std::vector<double> theta;
    std::vector<double> dtheta;
    std::vector<double> u;
    char delimiter = ' ';
    while ( std::getline( csv_file , line ) )
    {
        std::istringstream isline(line);
        std::getline( isline , s_x , delimiter );
        std::getline( isline , s_dx , delimiter );
        std::getline( isline , s_theta , delimiter );
        std::getline( isline , s_dtheta , delimiter );
        std::getline( isline , s_u , delimiter );
        std::getline( isline , to_throw , delimiter );
        x.push_back(std::stod(s_x));
        dx.push_back(std::stod(s_dx));
        theta.push_back(std::stod(s_theta));
        dtheta.push_back(std::stod(s_dtheta));
        u.push_back(std::stod(s_u));
        num_of_points++;
    }
    csv_file.close();
    gpcxx::regression_training_data< double , 5 > data;
    data.y.resize( num_of_points -1);
    data.x[0].resize( num_of_points -1);
    data.x[1].resize( num_of_points -1);
    data.x[2].resize( num_of_points -1);
    data.x[3].resize( num_of_points -1);
    data.x[4].resize( num_of_points -1);
    for(size_t i = 0; i < (num_of_points -1); ++i )
    {
        data.x[0][i] = x[i];
        data.x[1][i] = dx[i];
        data.x[2][i] = theta[i];
        data.x[3][i] = dtheta[i];
        data.x[4][i] = u[i];
        data.y[i] = theta[i+1];
    }
    return data;
}
}
#endif // GPCXX_IO_READ_DATA_HPP_INCLUDED